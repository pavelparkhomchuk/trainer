#!/bin/sh

celery -A trainer worker -l info -c ${CELERY_NUM_WORKERS}
