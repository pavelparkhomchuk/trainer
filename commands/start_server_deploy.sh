#!/bin/sh

python manage.py migrate
python manage.py collectstatic --noinput

gunicorn -w ${WSGI_WORKERS} -b 0:${WSGI_PORT} --chdir ./trainer trainer.wsgi:application --log-level=${WSGI_LOG_LEVEL}
