#!/bin/sh

python manage.py migrate --settings=trainer.settings.${MODE}
python manage.py collectstatic --noinput --settings=trainer.settings.${MODE}
python manage.py runserver --settings=trainer.settings.${MODE} 0:${WSGI_PORT}
