from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import routers, permissions
from api.views import (
    UserViewSet,
    TrainingListView,
    TrainingCreateView,
    TrainingUpdateView,
    TrainingDeleteView,
)

# router = routers.DefaultRouter()
# router.register(r'custom-users', UserViewSet)

schema_view = get_schema_view(
    openapi.Info(
        title="Snippets API",
        default_version="v1",
        description="Test description",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    # path("", include(router.urls)),
    # path("auth/", include("rest_framework.urls")),
    path("docs/", schema_view.with_ui("swagger", cache_timeout=0), name="swagger_docs"),
    path("auth-all/", include("djoser.urls")),
    path("auth/", include("djoser.urls.jwt")),
    path("all_trainings/", TrainingListView.as_view(), name="all_trainings"),
    path("create_training/", TrainingCreateView.as_view(), name="create_training"),
    path(
        "update_training/<int:pk>/",
        TrainingUpdateView.as_view(),
        name="update_training",
    ),
    path(
        "delete_training/<int:pk>/",
        TrainingDeleteView.as_view(),
        name="delete_training",
    ),
]
