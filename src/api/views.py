from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import (
    RetrieveAPIView,
    ListAPIView,
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView,
)

from api.serializers import UserSerializer, TrainingSerializer, ProgramSerializer
from users.models import CustomUser, Training, Program, UserProperties


class UserViewSet(ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer
    permission_classes = [AllowAny]


# Create
class TrainingCreateView(CreateAPIView):
    serializer_class = TrainingSerializer


# Read
class TrainingListView(ListAPIView):
    queryset = Training.objects.all()
    serializer_class = TrainingSerializer
    permission_classes = [AllowAny]


# Update
class TrainingUpdateView(UpdateAPIView):
    serializer_class = TrainingSerializer

    def get_queryset(self):
        return Training.objects.filter(id=self.kwargs["pk"])


class TrainingDeleteView(DestroyAPIView):
    serializer_class = TrainingSerializer

    def get_queryset(self):
        return Training.objects.filter(id=self.kwargs["pk"])
