from django.contrib import admin

from purchases.models import Cart, Product


@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    pass


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass
