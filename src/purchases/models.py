from django.contrib.auth import get_user_model
from django.db import models


class Cart(models.Model):
    user = models.OneToOneField(
        to=get_user_model(), null=False, on_delete=models.CASCADE
    )
    products = models.ManyToManyField(to="Product")

    def __str__(self):
        return f"Cart {self.user}"


class Product(models.Model):
    name = models.CharField(max_length=100)
    price = models.PositiveIntegerField()

    def __str__(self):
        return f"{self.name} ${self.price}"
