from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.urls import path

from purchases.views import CartView, ProductDelete, AddProductToCart, ProductsView

urlpatterns = [
    path("cart/", CartView.as_view(), name="cart"),
    path("product_delete/<int:id>", ProductDelete.as_view(), name="product_delete"),
    path("products_list", ProductsView.as_view(), name="products_list"),
    path(
        "add_product_to_cart/<int:id>", AddProductToCart.as_view(), name="product_add"
    ),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
