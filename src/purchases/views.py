from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, DeleteView, RedirectView, TemplateView

from purchases.models import Cart, Product


class CartView(ListView):
    template_name = "cart.html"
    context_object_name = "carts"

    def get_queryset(self):
        queryset = Cart.objects.filter(user=self.request.user)
        return queryset


class ProductsView(ListView):
    template_name = "products.html"
    context_object_name = "products"

    def get_queryset(self):
        products = Product.objects.all()
        return products


class AddProductToCart(RedirectView):
    def get(self, request, *args, **kwargs):
        cart = Cart.objects.get(user=request.user)
        product = Product.objects.get(id=kwargs.get("id"))
        cart.products.add(product)

        return super().get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return reverse_lazy("products_list")


class ProductDelete(RedirectView):
    def get(self, request, *args, **kwargs):
        cart = Cart.objects.get(user=request.user)
        product = Product.objects.get(id=kwargs.get("id"))
        cart.products.remove(product)
        return super().get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return reverse_lazy("cart")
