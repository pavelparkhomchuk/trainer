from django.contrib.auth.forms import UserCreationForm
from users.models import CustomUser as User, UserProperties
from django.forms import EmailField, ModelForm


class RegistrationForm(UserCreationForm):

    email = EmailField(
        max_length=200, help_text="registration without email is not possible!"
    )

    class Meta:
        model = User
        fields = [
            "email",
            "password1",
            "password2",
        ]


class UserPropertiesForm(ModelForm):
    class Meta:
        model = UserProperties
        fields = [
            "sex",
            "birthdate",
            "height",
            "weight",
            "lifestyle",
            "goal",
        ]

    def __init__(self, user, *args, **kwargs):
        super(UserPropertiesForm, self).__init__(*args, **kwargs)
        self.user = user

    def save(self, commit=True):
        obj = super(UserPropertiesForm, self).save(commit=False)
        obj.user = self.user
        if commit:
            obj.save()
        return obj
