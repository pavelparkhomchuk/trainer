import uuid
import datetime
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db import models
from users.managers import CustomUserManager

from .validators import birthdate_validator


# Create your models here.
class CustomUser(AbstractUser):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    username = None
    email = models.EmailField(
        _("email adress"),
        unique=True,
        max_length=150,
        help_text=_(
            "Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."
        ),
        error_messages={
            "unique": _("A user with that username already exists."),
        },
    )
    first_name = models.CharField(_("first name"), max_length=150, blank=True)
    last_name = models.CharField(_("last name"), max_length=150, blank=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)

    objects = CustomUserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __str__(self):
        return f"{self.email}"


class UserProperties(models.Model):
    user = models.OneToOneField(
        CustomUser, on_delete=models.CASCADE, null=True, blank=True
    )
    sex = models.CharField(
        null=False, choices=(("M", "Male"), ("F", "Female")), max_length=1
    )
    birthdate = models.DateField(
        null=False, default=datetime.date.today, validators=[birthdate_validator]
    )
    height = models.IntegerField(
        null=False, validators=[MinValueValidator(100), MaxValueValidator(230)]
    )
    weight = models.IntegerField(
        null=False, validators=[MinValueValidator(30), MaxValueValidator(250)]
    )
    lifestyle = models.CharField(
        null=False,
        choices=(("A", "Active"), ("M", "Medium"), ("P", "Passive")),
        default="M",
        max_length=2,
    )
    goal = models.CharField(
        null=False,
        choices=(
            ("GW", "Gain weight"),
            ("MW", "Maintaining weight"),
            ("LW", "Loss weight"),
        ),
        default="MW",
        max_length=2,
    )
    calories = models.IntegerField(null=True, editable=False)
    program = models.ForeignKey(
        "Program", on_delete=models.CASCADE, null=True, editable=False
    )

    def save(self, *args, **kwargs):
        self.calories = self.count_calories()
        self.program = self.get_program()
        super().save(*args, **kwargs)

    def get_age(self):
        return datetime.datetime.now().year - self.birthdate.year

    def count_calories(self):
        if self.sex == "M":
            calories = (
                10.0 * float(self.weight)
                + 6.25 * float(self.height)
                - 5 * float(self.get_age())
                + 5.0
            )
        else:
            calories = (
                10.0 * float(self.weight)
                + 6.25 * float(self.height)
                - 5 * float(self.get_age())
                - 161.0
            )

        if self.goal == "GW":
            calories += 500.0
        elif self.goal == "LW":
            calories -= 500.0

        return int(calories)

    def get_program(self):
        program = None
        if self.goal == "GW":
            program = Program.objects.get(for_goal="GW")
        elif self.goal == "LW":
            program = Program.objects.get(for_goal="LW")
        else:
            program = Program.objects.get(for_goal="MW")
        return program


class Program(models.Model):
    id = models.SmallAutoField(primary_key=True)
    name = models.CharField(max_length=100)
    for_goal = models.CharField(
        null=False,
        choices=(
            ("GW", "Gain weight"),
            ("MW", "Maintaining weight"),
            ("LW", "Loss weight"),
        ),
        default="MW",
        max_length=2,
    )
    description = models.CharField(null=True, max_length=1000)
    video_introduction = models.CharField(
        null=True, blank=True, max_length=1000
    )  # Use only embed links

    def __str__(self):
        return f"{self.name}"


class Training(models.Model):
    id = models.SmallAutoField(primary_key=True)
    program = models.ForeignKey(Program, on_delete=models.CASCADE, null=False)
    muscle_groups = models.CharField(
        null=False, blank=True, max_length=100, default="Full body"
    )
    rest = models.PositiveIntegerField(
        null=False, validators=[MinValueValidator(10), MaxValueValidator(180)]
    )
    exercises = models.ManyToManyField("Exercise")

    def __str__(self):
        return f"{self.program.name} for {self.muscle_groups}"


class Exercise(models.Model):
    id = models.AutoField(primary_key=True)
    sets = models.IntegerField(null=True, blank=True)
    reps = models.IntegerField(null=True, blank=True)
    name = models.CharField(null=False, max_length=32)
    description = models.CharField(null=True, max_length=1000)
    video_link = models.CharField(null=True, max_length=1000)

    def __str__(self):
        return f"{self.name}"
