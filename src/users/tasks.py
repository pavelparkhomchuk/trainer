import time
from requests import request
from celery import shared_task
from django.core.mail import send_mail


@shared_task
def celery_test(n):
    time.sleep(n)


@shared_task
def send_email_with_currency():
    data = request(
        "GET", "https://api.minfin.com.ua/mb/47f6859738087488af9a8fe5f4bb2f2bafe5e89a/"
    ).json()
    for item in data:
        if item["currency"] == "usd":
            currency_data = item

    send_mail(
        "USD exchange rate",
        f'USD exchange rate on {currency_data["pointDate"]} is {currency_data["ask"]}/{currency_data["bid"]}',
        "from@example.com",
        ["mikolaz2727@gmail.com", "pavelparkhomchuk@gmail.com"],
        fail_silently=False,
    )
