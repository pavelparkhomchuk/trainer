from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.urls import path

from users.views import (
    IndexView,
    ProgramDetailView,
    TrainingView,
    ExerciseView,
    RegistrationView,
    UserLoginView,
    UserLogoutView,
    UserPropertiesView,
    ProgramsView,
)

urlpatterns = [
    path("", IndexView.as_view(), name="main"),
    path("registration/", RegistrationView.as_view(), name="registration"),
    path("login/", UserLoginView.as_view(), name="login"),
    path("logout/", UserLogoutView.as_view(), name="logout"),
    path("properties/", UserPropertiesView.as_view(), name="properties"),
    path("programs/", ProgramsView.as_view(), name="programs"),
    path("program_detail/<int:id>", ProgramDetailView.as_view(), name="program_detail"),
    path("training/<int:program_id>", TrainingView.as_view(), name="training"),
    path("exercise/<int:exercise_id>", ExerciseView.as_view(), name="exercise"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
