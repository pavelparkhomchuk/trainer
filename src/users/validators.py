import datetime

from django.core.exceptions import ValidationError


def birthdate_validator(date):
    if datetime.datetime.now().year - date.year < 16:
        raise ValidationError("User should be at least  16 years old")
