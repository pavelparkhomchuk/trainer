from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, DetailView, CreateView

from users.forms import RegistrationForm, UserPropertiesForm
from users.models import UserProperties, Program, Training, Exercise
from users.tasks import celery_test, send_email_with_currency


class RegistrationView(CreateView):
    template_name = "registration.html"
    form_class = RegistrationForm
    success_url = reverse_lazy("main")


class UserLoginView(LoginView):
    template_name = "login.html"
    redirect_authenticated_user = reverse_lazy("main")


class UserLogoutView(LogoutView):
    template_name = "index.html"


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        current_user_id = self.request.user
        if (
            self.request.user.is_authenticated
            and UserProperties.objects.filter(user=current_user_id).exists()
        ):
            program_id = UserProperties.objects.values_list(
                "program_id", flat=True
            ).get(user_id=current_user_id)
            user_program = Program.objects.get(id=program_id)
            user_calories = UserProperties.objects.values_list(
                "calories", flat=True
            ).get(user_id=current_user_id)
            queryset = {
                "user_program": user_program,
                "user_calories": user_calories,
            }
            return queryset


class ProgramsView(ListView):
    template_name = "programs.html"
    paginate_by = 1

    def get_queryset(self):
        all_programs = Program.objects.all()
        return all_programs


class UserPropertiesView(CreateView):
    template_name = "properties.html"
    form_class = UserPropertiesForm
    success_url = reverse_lazy("main")

    def get_form_kwargs(self):
        kwargs = super(UserPropertiesView, self).get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs


class ProgramDetailView(DetailView, LoginRequiredMixin):
    template_name = "program_detail.html"
    model = Program

    def get_object(self, queryset=None):
        return Program.objects.get(id=self.kwargs.get("id"))


class TrainingView(ListView, LoginRequiredMixin):
    template_name = "training.html"
    context_object_name = "trainings"

    def get_queryset(self):
        return Training.objects.filter(program_id=self.kwargs.get("program_id"))


class ExerciseView(DetailView, LoginRequiredMixin):
    model = Exercise
    template_name = "exercise.html"
    context_object_name = "exercise"

    def get_object(self, queryset=None):
        return Exercise.objects.get(id=self.kwargs.get("exercise_id"))


def celery_test_view(request):
    n = 10
    celery_test.delay(n)

    return HttpResponse(f"Will finish in {n} seconds")


def usd_exchange_email(request):
    send_email_with_currency.delay()

    return HttpResponse("WAIT FOR EMAIL")
